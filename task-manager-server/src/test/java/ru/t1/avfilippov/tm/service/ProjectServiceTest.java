package ru.t1.avfilippov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.service.IConnectionService;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.exception.AbstractException;
import ru.t1.avfilippov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.dto.model.ProjectDTO;

import static ru.t1.avfilippov.tm.constant.TestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final ProjectService service = new ProjectService(connectionService);

    private static ProjectDTO userProject1;

    private static ProjectDTO userProject2;

    private static ProjectDTO adminProject1;


    @Before
    public void before() throws UserIdEmptyException, ProjectNotFoundException {
        userProject1 = service.add(USER1.getId(), USER_PROJECT1);
        userProject2 = service.add(USER1.getId(), USER_PROJECT2);
        adminProject1 = service.add(USER2.getId(), ADMIN_PROJECT1);
    }

    @After
    public void after() throws UserIdEmptyException {
        service.clear(USER1.getId());
    }

    @Test
    public void add() throws UserIdEmptyException, ProjectNotFoundException {
        Assert.assertNotNull(service.add(USER_PROJECT2.getUserId(), USER_PROJECT2));
        Assert.assertThrows(Exception.class, () -> service.add(NULL_PROJECT.getUserId(), NULL_PROJECT));
    }

    @Test
    public void addByUserId() throws UserIdEmptyException, ProjectNotFoundException {
        Assert.assertNotNull(service.add(USER1.getId(), USER_PROJECT2));
    }

    @Test
    public void createByUserId() {
        Assert.assertEquals(adminProject1.getUserId(), USER2.getId());
    }

    @Test
    public void findByNullId() {
        Assert.assertNull(service.findOneById(USER1.getId(), null));
    }

    @Test
    public void updateById() throws AbstractException {
        Assert.assertNotNull(service.updateById(USER1.getId(), userProject1.getId(), "1", "2"));
    }

    @Test
    public void updateByIndex() throws AbstractException {
        Assert.assertNotNull(service.updateByIndex(USER1.getId(), 1, "3", "4"));
    }

    @Test
    public void changeStatusById() throws AbstractException {
        Assert.assertNotNull(service.changeProjectStatusById(USER1.getId(), userProject1.getId(), Status.IN_PROGRESS));
    }

    @Test
    public void changeStatusByIndex() throws AbstractException {
        Assert.assertNotNull(service.changeProjectStatusByIndex(USER1.getId(), 1, Status.IN_PROGRESS));
    }

    @Test
    public void removeStatusById() throws AbstractException {
        Assert.assertNotNull(service.removeById(USER1.getId(), userProject1.getId()));
    }

    @Test
    public void removeStatusByIndex() throws AbstractException {
        Assert.assertNotNull(service.removeByIndex(USER1.getId(), 1));
    }

    @Test
    public void removeAll() throws UserIdEmptyException {
        service.clear(USER1.getId());
        Assert.assertFalse(service.existsById(userProject1.getUserId(), userProject1.getId()));
    }

}
