package ru.t1.avfilippov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;

import java.util.Collection;
import java.util.List;

public interface ISessionService {

    @Nullable
    @SneakyThrows
    SessionDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    SessionDTO add(@Nullable SessionDTO model);

    @NotNull
    @SneakyThrows
    Collection<SessionDTO> add(@NotNull Collection<SessionDTO> models);

    @SneakyThrows
    void clear(@Nullable String userId);

    @NotNull
    @SneakyThrows
    SessionDTO remove(@NotNull String userId, @Nullable SessionDTO model);

    @SneakyThrows
    boolean existsById(@Nullable String id);

    @Nullable
    List<SessionDTO> findAll(@Nullable String userId);

}
